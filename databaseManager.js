'use strict';

const AWS = require('aws-sdk');
let dynamo = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = 'coa-parking-datatable';

module.exports.initializateDynamoClient = newDynamo => {
    dynamo = newDynamo;
};

module.exports.saveFacility = facility => {
    const payload = {
        'facilityid': facility.facilityid,
        'name': facility.name,
        'available': facility.available,
        'coords': facility.coords,
    };

    const params = {
        TableName: TABLE_NAME,
        Item: payload
    };

    return dynamo.put(params).promise().then(() => {
        return payload.facilityid;
    });
};

module.exports.getFacility = facilityid => {
    const params = {
        Key: {
            facilityid: facilityid
        },
        TableName: TABLE_NAME
    };

    return dynamo.get(params).promise().then(result => {
        return result.Facility;
    });
};

module.exports.deleteFacility = facilityid => {
    const params = {
        Key: {
            facilityid: facilityid
        },
        TableName: TABLE_NAME
    };

    return dynamo.delete(params).promise();
};

module.exports.updateFacility = (facilityid, paramsName, paramsValue) => {
    const params = {
        TableName: TABLE_NAME,
        Key: {
            facilityid: facilityid
        },
        ConditionExpression: 'attribute_exists(facilityid)',
        UpdateExpression: 'set #n = :v',
        ExpressionAttributeNames: {
            '#n': paramsName
        },
        ExpressionAttributeValues: {
            ':v': paramsValue
        },
        ReturnValues: 'ALL_NEW'
    };

    return dynamo.update(params).promise().then(response => {
        return response.Attributes;
    });
};