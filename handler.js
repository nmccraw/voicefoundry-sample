'use strict';
const aws = require('aws-sdk');
const s3 = new aws.S3();
const databaseManager = require('./databaseManager');


module.exports.update = (event, context, callback) => {

    const parkingDecks = [
        {'key': 'RANKINFacility', 'name': 'Rankin Ave', 'coords': [35.596133, -82.554072]},
        {'key': 'CIVICFacility', 'name': 'Civic Center', 'coords': [35.596867, -82.554126]},
        {'key': 'WALLFacility', 'name': 'Wall Street', 'coords': [35.594614, -82.557025]},
        {'key': 'BILTMOREFacility', 'name': 'Biltmore Ave', 'coords': [35.592445, -82.551773]}
    ];
    const bucket = event.Records[0].s3.bucket.name;
    const key = event.Records[0].s3.object.key;

    console.log(bucket, key);

    const params = {
        Bucket: bucket,
        Key: key,
    };
    s3.getObject(params, function (err, data) {
        if (err) console.log(err, err.stack);
        else {
            let updates = {'decks': []};
            const textString = data.Body.toString().replace(/[\r\n]+/g, '').replace(/\s{1,10}/g, ' ');
            const array = textString.split(' ');

            for (let i = 0; i < parkingDecks.length; i++) {
                console.log(parkingDecks[i].key);
                databaseManager.getFacility(parkingDecks[i].key).then(response => {
                    if (!response) {
                        let facility = {
                            'facilityid': parkingDecks[i].key,
                            'name': parkingDecks[i].name,
                            'available': array[array.indexOf(parkingDecks[i].key) + 3],
                            'coords': parkingDecks[i].coords,
                        };
                        databaseManager.saveFacility(facility).then(response => {
                            console.log(response);
                        })
                    } else {
                        databaseManager.updateFacility(parkingDecks[i].key, 'name', parkingDecks[i].name).then(response => {
                            console.log(response);
                        });
                        databaseManager.updateFacility(parkingDecks[i].key, 'available', array[array.indexOf(parkingDecks[i].key) + 3]).then(response => {
                            console.log(response);
                        });
                        databaseManager.updateFacility(parkingDecks[i].key, 'coords', parkingDecks[i].coords).then(response => {
                            console.log(response);
                        });
                    }
                });

            }
            ;

            console.log('A new file ' + key + ' was created in the bucket ' + bucket);
            callback(null);
        }
    });
}


