#Parking Garage Availability Parsing

I try to  follow the  philosophy that you always solve more than one problem at a time. This is little project has been in my team's backlog for a while. A brief overview is that we have near real-time availability information from the various City of Asheville and Buncombe County Parking garages that we collect and display on a simple web interface. There are a few different vendors for the availability data so we have a few different types of streams that need to be parsed, normalized, then displayed.

Currently, everything is collated into a series of JSON documents in s3 buckets that the web interface pulls from. We want to actually database the data so we can build out  mobile and voice  interfaces.

## How to use

Once deployed, it is looking for a .dat file in the s3 trigger bucket, **coa-parking-datastore**. Two example files are included. It will parse out the relevant data from the file and insert or update it into the database. 

## Things that need work

- This is the first time I  have worked with Serverless framework in a while and the  first time in a  few months I've messed  with   Cloudformation-esque templates. I need to brush up on creating and assigning IAM roles and policies in Cloudformation again. For this example, where  my execution role didn't have the proper permissions, I added  them through the  console.  I've been working pretty heavily with Terraform. I would be willing to build this stack with Terraform to prove that I do understand the concepts.

- I've been working in Postgres  and MariaDB for  the  last few years. I'm sure that  my Nosql Dynamo table could use some tuning. 

- I would go  through and parameterize the table and bucket names and maybe make my insert database function dynamic so that the function just natural handles schema changes. 

- I  don't have tests on  my code. 